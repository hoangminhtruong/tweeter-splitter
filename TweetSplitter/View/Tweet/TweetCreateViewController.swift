//
//  TweetCreateViewController.swift
//  TweetSplitter
//
//  Created by Hoang Minh Truong on 7/10/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TweetCreateViewController: UIViewController {

    @IBOutlet weak var lblHint: UILabel!
    @IBOutlet weak var txtMessage: UITextView!
    
    private let disposeBage = DisposeBag()
    private var viewModel: TweetCreateViewModel?
    var messages: BehaviorRelay<[String]> = BehaviorRelay<[String]>.init(value: [])
    
    // MARK: - View's LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupViews()
        // Do any additional setup after loading the view.
    }
    
    // MARK: Config View
    
    func setupViews() {
        viewModel = TweetCreateViewModel()
        txtMessage.rx.text
            .map{text -> Bool in
                return !text!.isEmpty
            }
            .bind(to: lblHint.rx.isHidden)
            .disposed(by: disposeBage)
    }
    
    func setupNavigationBar() {
        // Set Title
        self.navigationItem.title = "Post Tweet"
        
        // Add Left bar button
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"icClose"), style: .plain, target: self, action: #selector(tapCloseTweet))
        
        // Add Right bar button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Post", style: .plain, target: self, action: #selector(tapTweet))
        
    }
    
    @objc func tapCloseTweet() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapTweet() {
        // Validate message
        let result = viewModel?.validator(message: txtMessage.text)
        switch result {
        case .success(let newMessages)?:
            messages.accept(newMessages)
            self.dismiss(animated: true, completion: nil)
        case .failure(let err)?:
            Utils.showAlert(title: "Alert", message: err.rawValue, viewController: self)
        case .none:
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
