//
//  TweetCell.swift
//  TweetSplitter
//
//  Created by Mac on 7/11/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TweetCell: UITableViewCell {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblCountMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    var tweetViewModel: TweetHomeViewModel?{
        didSet {
            lblMessage.text = tweetViewModel?.message
            lblCountMessage.text = tweetViewModel?.countMessage
            lblTime.text = tweetViewModel?.date.timeAgo()
        }
    }
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
