//
//  TweetHomeViewController.swift
//  TweetSplitter
//
//  Created by Hoang Minh Truong on 7/10/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TweetHomeViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    private var viewModel: TweetHomeViewModels?
    private let disposeBag: DisposeBag = DisposeBag()
    private let cellId = "TweetCell"
    
    // MARK: - View's LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupViews()
        bindingData()
        // Do any additional setup after loading the view.
    }
    
    // MARK: Config View
    
    private func setupViews() {
        viewModel = TweetHomeViewModels()
        tblView.separatorStyle = .none
        tblView.register(UINib(nibName: cellId, bundle: Bundle.main), forCellReuseIdentifier: cellId)
    }
    
    private func setupNavigationBar() {
        // Set Title
        self.navigationItem.title = "Home"
        
        // Add Right bar button
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"icTweet"), style: .plain, target: self, action: #selector(tapCreateTweet))
        
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    @objc func tapCreateTweet() {
        let vc = UIStoryboard.tweetCreateViewController()
        vc?.messages
            .skip(1)
            .subscribe({ (value) in
            guard let message = value.element else {
                return
            }
            self.viewModel?.addChunk(messages: message)
        })
        .disposed(by: disposeBag)
        self.present(UINavigationController(rootViewController: vc!), animated: true, completion: nil)
    }
    
    // MARK: - Binding Data
    
    private func bindingData() {
        // binding viewmodel tweets to the tableview
        
        viewModel?.tweets
            .bind(to: tblView.rx.items(cellIdentifier: cellId)) { row, model, cell in
                let cell = cell as! TweetCell
                cell.tweetViewModel = model
            }
            .disposed(by: disposeBag)
    }

}
