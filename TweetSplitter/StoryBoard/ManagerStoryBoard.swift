//
//  ManagerStoryBoard.swift
//  TweetSplitter
//
//  Created by Hoang Minh Truong on 7/10/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    //MARK: Tweet Storyboard
    class func tweetStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Tweet", bundle: Bundle.main)
    }
    
    class func tweetHomeViewController() -> TweetHomeViewController? {
        return tweetStoryboard().instantiateViewController(withIdentifier: "TweetHomeViewController") as? TweetHomeViewController
    }
    
    class func tweetCreateViewController() -> TweetCreateViewController? {
        return tweetStoryboard().instantiateViewController(withIdentifier: "TweetCreateViewController") as? TweetCreateViewController
    }
    
}
