//
//  Int+Extension.swift
//  TweetSplitter
//
//  Created by Hoang Minh Truong on 7/10/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation

extension Int {
    var numberOfDigits: Int {
        return String(self).count
    }
}
