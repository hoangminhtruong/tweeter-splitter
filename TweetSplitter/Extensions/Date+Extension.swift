//
//  Date+Extension.swift
//  TweetSplitter
//
//  Created by Hoang Minh Truong on 7/11/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation

extension Date {
    
    // Get time ago at newsfeed
    func timeAgo() -> String {
        let calendar = Calendar.current
        let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
        let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
        let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!
        let weekAgo = calendar.date(byAdding: .day, value: -7, to: Date())!
        
        if minuteAgo < self {
            let diff = Calendar.current.dateComponents([.second], from: self, to: Date()).second ?? 0
            return "\(diff) \(diff == 1 ? "second": "seconds") ago"
        } else if hourAgo < self {
            let diff = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
            return "\(diff) \(diff == 1 ? "minute": "minutes") ago"
        } else if dayAgo < self {
            let diff = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
            return "\(diff) \(diff == 1 ? "hour": "hours") ago"
        } else if weekAgo < self {
            let diff = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0
            return "\(diff) \(diff == 1 ? "day": "days") ago"
        }
        let diff = Calendar.current.dateComponents([.weekOfYear], from: self, to: Date()).weekOfYear ?? 0
        return "\(diff) \(diff == 1 ? "week": "weeks") ago"
    }
}
