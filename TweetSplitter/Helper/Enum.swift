//
//  Enum.swift
//  TweetSplitter
//
//  Created by Hoang Minh Truong on 7/10/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//


import Foundation

//MARK: - Message Error
enum MessageError: String, Equatable {
    case emptyOrOnlyWhiteSpace = "Please enter your thinking"
    case haveTooLongWord = "Please check your message. Maybe it contains a word too long"
}

//MARK: - Message Split Status
enum SplitResult {
    case success([String])
    case failure(MessageError)
}
