//
//  TweetSplitter.swift
//  TweetSplitter
//
//  Created by Hoang Minh Truong on 7/10/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation

class TweetSplitter {
    
    // Singleton
    static let sharedInstance = TweetSplitter()
    // Set Max Lenght of line message
    private let maxChunk: Int = 50
    
    // Split Message will be return result Failure or Success. It's easy to used for Unittest
    func splitMessage(message: String) -> SplitResult {
        //Remove whitespaces
        let actualyMessage = message.trimmingCharacters(in: .whitespaces)
        let countCharacters  = actualyMessage.count
        
        // If message have only whitespace or empty
        if actualyMessage.isEmpty {
            return .failure(.emptyOrOnlyWhiteSpace)
        }
        
        // If message have length less than or equal 50 characters
        if countCharacters <= maxChunk {
            return .success([actualyMessage])
        }
        
        // If message have length greater than 50 characters
        
        // Estimate total of lines
        let estimateLines = ((countCharacters - 1) / maxChunk) + 1
        let arrMessages = actualyMessage.components(separatedBy: " ")
        // Estimate number digit of lines
        var digitEstimateLines = estimateLines.numberOfDigits
        var arrSplitMessage: [String] = []
        var finishSplit = false
        
        while finishSplit == false {
            // Split message to array width estimate lines
            // Reset array arrSplitMessage and recalculate if wrong first split
            arrSplitMessage = []
            var strMessage = ""
            for str in arrMessages {
                // Check count character of line exist or not, if not exist return [] let user know have string more than 50 - x characters
                
                if (str.count > maxChunk -  ((arrSplitMessage.count + 1).numberOfDigits + digitEstimateLines + 2)) {
                    return .failure(.haveTooLongWord)
                }
                if strMessage == "" {
                    strMessage = str
                } else {
                    // Check if final line message plus next str with condition length <= 50 characters
                    if (str + " " + strMessage).count > maxChunk - ((arrSplitMessage.count + 1).numberOfDigits + digitEstimateLines + 2) {
                        // Append final line message to Array Messages and set firt message for next line
                        arrSplitMessage.append(strMessage)
                        strMessage = str
                    } else {
                        // Concat final message with next string
                        strMessage += " " + str
                    }
                }
            }
            
            // Append last message split when end of loop
            arrSplitMessage.append(strMessage)

            if arrSplitMessage.count.numberOfDigits == digitEstimateLines {
                // If digit of array messages equal digit of estimate lines so number of line is right. End of calculates
                finishSplit = true
            } else {
                // If digit of array messages larger than digit of estimate lines, recalculate line
                digitEstimateLines += 1
            }
        }
        
        // Map array message with part indicator
        let chunks = arrSplitMessage.indices.map({ (index : Int) -> String in
            return "\(index + 1)/\(arrSplitMessage.count) " + arrSplitMessage[index]
        })
        
        return .success(chunks)
    }
}
