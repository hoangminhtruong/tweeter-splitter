//
//  TweetModel.swift
//  TweetSplitter
//
//  Created by Hoang Minh Truong on 7/10/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation

class TweetModel: NSObject {
    var message: String
    var date: Date
    
    init(message: String) {
        self.message = message
        self.date = Date()
    }
}
