//
//  TweetCreateViewModel.swift
//  TweetSplitter
//
//  Created by Hoang Minh Truong on 7/10/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation
import RxSwift

class TweetCreateViewModel {
    
    //MARK: Validator
    func validator(message: String) -> SplitResult {
        return TweetSplitter.sharedInstance.splitMessage(message: message)
    }
}
