//
//  TweetHomeViewModel.swift
//  TweetSplitter
//
//  Created by Hoang Minh Truong on 7/10/19.
//  Copyright © 2019 Hoang Minh Truong. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class TweetHomeViewModels {
    var tweets: BehaviorRelay = BehaviorRelay<[TweetHomeViewModel]>(value: [])
    
    func addChunk(messages: [String]) {
        let newViewModel = messages.map{ message -> TweetHomeViewModel in
            return TweetHomeViewModel(tweetModel: TweetModel(message: message))
        }
        tweets.accept(newViewModel + tweets.value)
    }
}

class TweetHomeViewModel {
    var message: String
    var countMessage: String
    var date: Date
    
    init(tweetModel: TweetModel) {
        self.message = tweetModel.message
        self.countMessage = "\(tweetModel.message.count)/50"
        self.date = tweetModel.date
    }
}
